package inheritance;

public class Otobus extends Tasit {
	
	private int yolcuKapasitesi;

	public int getYolcuKapasitesi() {
		return yolcuKapasitesi;
	}

	public void setYolcuKapasitesi(int yolcuKapasitesi) {
		this.yolcuKapasitesi = yolcuKapasitesi;
	}
	
	public void otobusBilgiYaz(){
		System.out.println("-----------Otobus------------");
		System.out.println("Otob�s�n yolcu kapasitesi : " +yolcuKapasitesi);
	}
}
