package inheritance;

public class TasitTes {

	public static void main(String[] args) {
		
		Otomobil myOtomobil = new Otomobil();
		Otobus myOtobus = new Otobus();
		
		myOtomobil.setMarka("BMW");
		myOtomobil.setModel(2012);
		myOtomobil.setMaxHiz(200);
		myOtomobil.setYolcuSayisi(5);
		
		myOtobus.setMarka("Mercedes");
		myOtobus.setModel(2010);
		myOtobus.setYolcuKapasitesi(42);
		
		myOtomobil.otomobilBilgiYaz();
		myOtomobil.tasitBilgiYazdir();
		
		myOtobus.otobusBilgiYaz();
		myOtobus.tasitBilgiYazdir();
		
	}

}
