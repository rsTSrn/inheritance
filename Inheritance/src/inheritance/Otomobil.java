package inheritance;

public class Otomobil extends Tasit{

	private int maxHiz;
	private int yolcuSayisi;
	
	public int getYolcuSayisi() {
		return yolcuSayisi;
	}
	public void setYolcuSayisi(int yolcuSayisi) {
		this.yolcuSayisi = yolcuSayisi;
	}
	public int getMaxHiz() {
		return maxHiz;
	}
	public void setMaxHiz(int maxHiz) {
		this.maxHiz = maxHiz;
	}
	
	public void otomobilBilgiYaz(){
		System.out.println("-----------Otomobil------------");
		System.out.println("Otomobilin maxHiz : " + maxHiz);
		System.out.println("Yolcu sayisi : " + yolcuSayisi);
	}
	
	
}
