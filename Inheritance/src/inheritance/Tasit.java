package inheritance;

public class Tasit {

	private String marka;
	private int model;
	
	public int getModel() {
		return model;
	}
	public void setModel(int model) {
		this.model = model;
	}
	public String getMarka() {
		return marka;
	}
	public void setMarka(String marka) {
		this.marka = marka;
	}
	
	public void tasitBilgiYazdir(){
		
		System.out.println("Ta��t�n markas� : " + marka);
		System.out.println("Ta��t�n modeli : " +model);
	}
}
